﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CompareFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo f1;
            FileInfo f2;
            FileInfo f3;

            if (args.Length == 3)
            {
                f1 = new FileInfo(args[1]);
                f2 = new FileInfo(args[2]);
                f3 = new FileInfo(args[3]);
            }
            else
            {
                f1 = new FileInfo("file1.jpg");
                f2 = new FileInfo("file2.txt");
                f3 = new FileInfo("file2_copy.txt");
            }
            

            Console.WriteLine("=====Different files=====");
            Console.WriteLine("File1: {0}, File2: {1}\nBajty: {2}\nHash(MD5): {3}\nHash(SHA256): {4}\n", f1.Name, f2.Name, CompareByBytes(f1,f2), CompareByHash_MD5(f1,f2), CompareByHash_SHA256(f1,f2));
            Console.WriteLine("====Same files====");
            Console.WriteLine("File1: {0}, File2: {1}\nBajty: {2}\nHash(MD5): {3}\nHash(SHA256): {4}\n", f2.Name, f3.Name, CompareByBytes(f2, f3), CompareByHash_MD5(f2, f3), CompareByHash_SHA256(f2,f3));
            Console.WriteLine("======Fin=======");
        }

        //byte by byte
        static bool CompareByBytes(FileInfo file1, FileInfo file2)
        {
            if (file1.Length != file2.Length)
                return false;

            using(FileStream fs1 = file1.OpenRead())
            using(FileStream fs2 = file2. OpenRead())
            {
                for(int i = 0; i < file1.Length; i++)
                {
                    if (fs1.ReadByte() != fs2.ReadByte())
                        return false;
                }
            }

            return true;
        }
        
        //md5
        static bool CompareByHash_MD5(FileInfo file1, FileInfo file2)
        {
            byte[] file1Hash = MD5.Create().ComputeHash(file1.OpenRead());
            byte[] file2Hash = MD5.Create().ComputeHash(file2.OpenRead());

            for (int i = 0; i < file1Hash.Length; i++)
            {
                if (file1Hash[i] != file2Hash[i])
                    return false;
            }

            return true;
        }

        //sha256
        static bool CompareByHash_SHA256(FileInfo file1, FileInfo file2)
        {
            byte[] file1Hash = SHA256.Create().ComputeHash(file1.OpenRead());
            byte[] file2Hash = SHA256.Create().ComputeHash(file2.OpenRead());

            for (int i = 0; i < file1Hash.Length; i++)
            {
                if (file1Hash[i] != file2Hash[i])
                    return false;
            }

            return true;
        }
    }
}
